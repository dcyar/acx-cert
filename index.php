<?php 
    require 'vendor/autoload.php';

    require_once('src/pdf.php');
    
    use Jhon\Pdf\Pdf;

    $pdf = new Pdf();

    $pdf->make('Antonio Sanchez Fernandez', 'La Historia del Arte Griego: De Las Cicladas al Helenismo', '11.5', date("Y/m/d"));
?>