<?php

namespace Jhon\Pdf\Views;

class Certificado {

    // Return view template
    public function view($name, $course, $duration, $date)
    {
        return "
            <!DOCTYPE html>
            <html lang='en'>
            <head>
                <meta charset='UTF-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <meta http-equiv='X-UA-Compatible' content='ie=edge'>
                <title>Document</title>
                
                <style>
                    @import url('https://fonts.googleapis.com/css?family=Calistoga|Josefin+Slab:400,700|Parisienne&display=swap');
                    html, body {
                        background-image: url('./assets/images/background.jpg');
                        margin:0;
                        padding:0;
                    }
                    p {
                        padding:0;
                    }
                    .container {
                        margin: 20px auto;
                        width: 100%;
                        height: 90%;
                        vertical-align: center;
                        text-align: center;
                    }
                    #username {
                        font-family: 'Parisienne', cursive;
                        font-size: 55px;
                    }
                    .courseName {
                        font-family: 'Calistoga', cursive;
                        font-size: 25px;
                        color: #c96e00;
                        font-weight: bold;
                    }
                    .footer {
                        width: 100%;
                        text-align: right;
                        margin: 10px 60px 0 0;
                        font-weight: bold;
                        padding: 0;
                    }

                    .footer .firma {
                        width: 150px;
                    }
                </style>
            </head>
            <body>
                <div class='container'>
                    <div class='logo'>
                        <img src='./assets/images/logo.png' alt='My Company Logo' />
                    </div>
                    <div class='info'>
                        <p style='font-weight:bold;'>POR EL PRESENTE CERTIFICADO SE RECONOCE QUE:</p>
                        <p id='username'>" . $name . "</p>
                        <p>HA COMPLETADO CON EXITO EL CURSO ONLINE DE FILOSOFIA TITULADO</p>
                        <p class='courseName'>\"" . $course . "\"</p>
                        <p style='font-weight:bold;'>ESTE CURSO HA TENIDO UNA DURACION TOTAL DE</p>
                        <p style='font-weight:bold;'>" . $duration . " HORAS DE FORMACION TEORICA</p>    
                    </div>
                    <div class='footer'>
                        <p class='date'>" . $date . "</p>
                        <p>ARCUX TEAM</p>
                        <img src='./assets/images/firma.png' class='firma' alt='firma' />
                    </div>
                </div>
            </body>
            </html>
        ";
    }
}