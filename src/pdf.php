<?php

namespace Jhon\Pdf;

use \Dompdf\Dompdf;

use Jhon\Pdf\Views\Certificado;

class Pdf {
    
    // Se construye el pdf desde la instancia de Dompdf
    public function make($name, $course, $duration, $date)
    {
        $template = new Certificado();

        $html = $template->view($name, $course, $duration, $date);

        $dompdf = new Dompdf();

        $dompdf->loadHtml($html);

        // portrait
        $dompdf->setPaper('A4', 'landscape');

        $dompdf->render();

        $dompdf->stream("certificado.pdf", array("Attachment" => false));
    }
    // Nombre, curso, hora y fecha
}
